package com.ruoyi.auth;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * 认证授权中心
 * 
 * @author ruoyi
 */
@EnableRyFeignClients
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class RuoYiAuthApplication
{
    public static Logger log = LoggerFactory.getLogger(RuoYiAuthApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        long startTime = System.currentTimeMillis();
        log.info("=========================== 啟動 RuoYiAuthApplication ===========================");
        log.info("args:{}", JSON.toJSONString(args));
        SpringApplication application = new SpringApplication(RuoYiAuthApplication.class);
        Properties properties = new Properties();
        // 關閉無用打印
        properties.setProperty("logging.pattern.console", "");
        application.setDefaultProperties(properties);

        // 取得環境相關資訊
        ConfigurableApplicationContext configurableApplicationContext = application.run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        port = port == null ? "8080" : port;
        String nacosIP = env.getProperty("spring.cloud.nacos.discovery.server-addr");
        String applicationName = env.getProperty("spring.application.name");
        String datasourceUrl = env.getProperty("spring.datasource.dynamic.datasource.master.url");
        String username = env.getProperty("spring.datasource.dynamic.datasource.master.username");
        String password = env.getProperty("spring.datasource.dynamic.datasource.master.password");
        log.info("=========================== 啟動完成 耗时:{} ===========================\n"
                        + "\n\t服務名稱: {}\n"
                        + "\t外部訪問地址: http://{}:{}/\n"
                        + "\tSwagger官方文檔: http://{}:{}/swagger-ui/\n"
                        + "\tNacos訪問地址: http://{}:8848/nacos/\n"
                        + "\tMaster Datasource位置: {}\n"
                        + "\tMaster Datasource使用者資訊: {}:{}\n",
                (System.currentTimeMillis() - startTime), applicationName, ip, port, ip, port, nacosIP, datasourceUrl, username, password);
    }
}
