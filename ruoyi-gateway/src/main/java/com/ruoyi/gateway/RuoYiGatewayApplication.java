package com.ruoyi.gateway;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * 网关启动程序
 * 
 * @author ruoyi
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class RuoYiGatewayApplication {

    public static Logger log = LoggerFactory.getLogger(RuoYiGatewayApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        long startTime = System.currentTimeMillis();
        System.out.println("=========================== 啟動 RuoYiGatewayApplication ===========================");
        log.info("args:{}", JSON.toJSONString(args));

        SpringApplication application = new SpringApplication(RuoYiGatewayApplication.class);
        Properties properties = new Properties();
        // 關閉無用打印
        properties.setProperty("logging.pattern.console", "");
        application.setDefaultProperties(properties);

        // 取得環境相關資訊
        ConfigurableApplicationContext configurableApplicationContext = application.run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        port = port == null ? "8080" : port;
        String nacosIP = env.getProperty("spring.cloud.nacos.discovery.server-addr");
        String applicationName = env.getProperty("spring.application.name");
        String datasourceUrl = env.getProperty("spring.datasource.dynamic.datasource.master.url");
        String username = env.getProperty("spring.datasource.dynamic.datasource.master.username");
        String password = env.getProperty("spring.datasource.dynamic.datasource.master.password");
        log.info("=========================== 啟動完成 耗时:{} ===========================\n"
                        + "\n\t服務名稱: {}\n"
                        + "\t外部訪問地址: http://{}:{}/\n"
                        + "\tSwagger官方文檔: http://{}:{}/swagger-ui/\n"
                        + "\tNacos訪問地址: http://{}/nacos/\n"
                        + "\tMaster Datasource位置: {}\n"
                        + "\tMaster Datasource使用者資訊: {}:{}\n",
                (System.currentTimeMillis() - startTime), applicationName, ip, port, ip, port, nacosIP, datasourceUrl, username, password);
        log.info(env.getProperty("spring.autoconfigure.exclude"));
    }
}
