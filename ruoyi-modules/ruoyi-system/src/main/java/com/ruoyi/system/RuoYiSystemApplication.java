package com.ruoyi.system;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.ruoyi.common.security.annotation.EnableCustomConfig;
import com.ruoyi.common.security.annotation.EnableRyFeignClients;
import com.ruoyi.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

/**
 * 系统模块
 * 
 * @author ruoyi
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class RuoYiSystemApplication
{
    public static Logger log = LoggerFactory.getLogger(RuoYiSystemApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        long startTime = System.currentTimeMillis();
        log.info("=========================== 啟動 RuoYiSystemApplication ===========================");
        log.info("args:{}", JSON.toJSONString(args));
        SpringApplication application = new SpringApplication(RuoYiSystemApplication.class);
        Properties properties = new Properties();
        // 關閉無用打印
        properties.setProperty("logging.pattern.console", "");
        application.setDefaultProperties(properties);

        // 取得環境相關資訊
        ConfigurableApplicationContext configurableApplicationContext = application.run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        port = port == null ? "8080" : port;
        String nacosIP = env.getProperty("spring.cloud.nacos.discovery.server-addr");
        String applicationName = env.getProperty("spring.application.name");
        String datasourceUrl = env.getProperty("spring.datasource.dynamic.datasource.master.url");
        String username = env.getProperty("spring.datasource.dynamic.datasource.master.username");
        String password = env.getProperty("spring.datasource.dynamic.datasource.master.password");
        log.info("=========================== 啟動完成 耗时:{} ===========================\n"
                        + "\n\t服務名稱: {}\n"
                        + "\t外部訪問地址: http://{}:{}/\n"
                        + "\tSwagger官方文檔: http://{}:{}/swagger-ui/\n"
                        + "\tNacos訪問地址: http://{}/nacos/\n"
                        + "\tMaster Datasource位置: {}\n"
                        + "\tMaster Datasource使用者資訊: {}:{}\n",
                (System.currentTimeMillis() - startTime), applicationName, ip, port, ip, port, nacosIP, datasourceUrl, username, password);
    }
}
